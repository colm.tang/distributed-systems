import auldfellas.AFQService;
import core.ClientInfo;
import core.Constants;
import core.Quotation;
import core.QuotationService;
import org.junit.BeforeClass;
import org.junit.Test;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class AuldfellasUnitTest {
    private static Registry registry;
    private static AFQService afqService;   //added to prevent garbage collection from removing the service at random after exporting object

    @BeforeClass
    public static void setup() {
        afqService = new AFQService();

        try {
            registry = LocateRegistry.createRegistry(1099);
            QuotationService quotationService = (QuotationService)
                    UnicastRemoteObject.exportObject(afqService, 0);
            registry.bind(Constants.AULD_FELLAS_SERVICE, quotationService);
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }

    @Test
    public void connectionTest() throws Exception {
        QuotationService service = (QuotationService) registry.lookup(Constants.AULD_FELLAS_SERVICE);
        assertNotNull(service);
    }

    @Test
    public void generateQuotationTest() throws Exception {

        QuotationService testService = (QuotationService) registry.lookup(Constants.AULD_FELLAS_SERVICE);

        ClientInfo genQuoTest = new ClientInfo("Niki Collier", 'F', 43, 0, 5, "PQR254/1");
        Quotation actual = testService.generateQuotation(genQuoTest);
        Quotation tester = new Quotation("Auld Fellas Ltd.", "AF001000", 688.0);
        assertThat(actual, instanceOf(Quotation.class));    // tests returned object against class Quotation
        assertEquals(tester.company, actual.company);
        assertEquals(tester.reference, actual.reference);
        assertEquals(tester.price, actual.price, 600.00);
    }   /*delta is large since this is only testing quotation generation*/

}
