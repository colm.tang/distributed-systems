import core.ClientInfo;
import core.Constants;
import core.Quotation;
import core.QuotationService;
import girlpower.GPQService;
import org.junit.BeforeClass;
import org.junit.Test;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class GirlpowerUnitTest {
    private static Registry registry;
    private static GPQService gpqService;    //added to prevent garbage collection from removing the service at random after exporting object

    @BeforeClass
    public static void setup() {
        gpqService = new GPQService();
        try {
            registry = LocateRegistry.createRegistry(1099);
            QuotationService quotationService = (QuotationService)
                    UnicastRemoteObject.exportObject(gpqService, 0);
            registry.bind(Constants.GIRL_POWER_SERVICE, quotationService);
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }

    @Test
    public void connectionTest() throws Exception {
        QuotationService service = (QuotationService)
                registry.lookup(Constants.GIRL_POWER_SERVICE);
        assertNotNull(service);
    }

    @Test
    public void generateQuotationTest() throws Exception {
        QuotationService gpqService = new GPQService();

        ClientInfo genQuoTest = new ClientInfo("Niki Collier", 'F', 43, 0, 5, "PQR254/1");
        Quotation actual = gpqService.generateQuotation(genQuoTest);
        Quotation tester = new Quotation("Girl Power Inc.", "GP001000", 45.0);
        assertThat(actual, instanceOf(Quotation.class));
        assertEquals(tester.company, actual.company);
        assertEquals(tester.reference, actual.reference);
        assertEquals(tester.price, actual.price, 600.00);
    }   /*delta is large since this is only testing quotation generation*/
}
