# Colm Tang		16760705

# Introduction

Solution for practical 1: RMI-based Distribution.

# Running the Program

To run the program you run commands (in root folder):

1. `mvn compile`
2. `mvn package`
3. `docker-compose build`
4. `docker-compose up`
5. run `docker-compose down` to cleanup docker network and images.

# Output

The project displays a table with client info followed by quotations from three quotation services.
The three quotation services give a random price within a varying ranges depending on 
age,gender,penalty points and years of no claims.

# Changes made from Quoco.master -> Quoco.rmi

Server class files were changed from binding remote objects to a localhost registry to looking up a reference of broker and
running a newly added method registerService to get "object X"(in this case broker) to locally bind each quotation service.

Unit tests were made to test generateQuotation for each quotation service which tests the elements of the quotation returned
against expected results to be generated, and also uses "coreMatchers.instanceOf" to test if the returned object is of 
type Quotation.

Docker compose file dependencies were configured so broker is built first and client is built last.