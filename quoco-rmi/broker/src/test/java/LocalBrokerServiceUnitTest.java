import broker.LocalBrokerService;
import core.BrokerService;
import core.ClientInfo;
import core.Constants;
import core.Quotation;
import org.junit.BeforeClass;
import org.junit.Test;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class LocalBrokerServiceUnitTest {
    private static Registry registry;
    private static LocalBrokerService brokerService;  //added to prevent garbage collection from removing the service at random after exporting object

    @BeforeClass
    public static void setup() {
        brokerService = new LocalBrokerService();
        try {
            registry = LocateRegistry.createRegistry(1099);
            BrokerService aBrokerService = (BrokerService)
                    UnicastRemoteObject.exportObject(brokerService, 0);
            registry.bind(Constants.BROKER_SERVICE, aBrokerService);
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }

    @Test
    public void connectionTest() throws Exception {
        BrokerService service = (BrokerService) registry.lookup(Constants.BROKER_SERVICE);
        assertNotNull(service);
    }

    @Test
    public void generateQuotationTest() throws RemoteException, NotBoundException {
        BrokerService service = (BrokerService) registry.lookup(Constants.BROKER_SERVICE);
        List<Quotation> quotations = new LinkedList<Quotation>();
        ClientInfo info = new ClientInfo("Niki Collier", 'F', 43, 0, 5, "PQR254/1");

        quotations = brokerService.getQuotations(info);

        assertEquals(new ArrayList<Quotation>(), quotations);

    }
}
