package broker;

import core.BrokerService;
import core.ClientInfo;
import core.Quotation;
import core.QuotationService;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.util.LinkedList;
import java.util.List;

/**
 * Implementation of the broker service that uses the Service Registry.
 *
 * @author Rem
 */
public class LocalBrokerService implements BrokerService {
    public List<Quotation> getQuotations(ClientInfo info) throws RemoteException {
        List<Quotation> quotations = new LinkedList<Quotation>();
        Registry registry = null;
        registry = LocateRegistry.getRegistry(1099);

        for (String name : registry.list()) {
            if (name.startsWith("qs-")) {
                QuotationService service = null;
                try {
                    service = (QuotationService) registry.lookup(name);
                } catch (NotBoundException e) {
                    e.printStackTrace();
                }
                quotations.add(service.generateQuotation(info));
            }
        }

        return quotations;
    }

    public void registerService(String name, java.rmi.Remote service) throws RemoteException {    // locally connects object A to RMI registry through object B
        Registry registry = null;
        registry = LocateRegistry.getRegistry(1099);
        try {
            registry.bind(name, service);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
