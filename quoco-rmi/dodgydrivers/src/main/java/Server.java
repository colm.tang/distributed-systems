import core.BrokerService;
import core.Constants;
import core.QuotationService;
import dodgydrivers.DDQService;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

public class Server {
    public static void main(String[] args) {
        QuotationService ddqService = new DDQService();
        try {
            // Connect to the RMI Registry - creating the registry will be the
            // responsibility of the broker.
            Registry registry = null;
            if (args.length == 0) {
                registry = LocateRegistry.createRegistry(1099);
            } else {
                registry = LocateRegistry.getRegistry(args[0]);
            }
            // Create the Remote Object
            QuotationService quotationService = (QuotationService)
                    UnicastRemoteObject.exportObject(ddqService, 0);
            // Register the object with the RMI Registry

            BrokerService lookupX = (BrokerService) registry.lookup(Constants.BROKER_SERVICE);  // use this instead of registry.bind because of restrictions on RMI over docker
            lookupX.registerService(Constants.DODGY_DRIVERS_SERVICE, quotationService);

            //registry.bind(Constants.DODGY_DRIVERS_SERVICE, quotationService);

            System.out.println("STOPPING SERVER SHUTDOWN");
            while (true) {
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }
}
