import core.ClientInfo;
import core.Constants;
import core.Quotation;
import core.QuotationService;
import dodgydrivers.DDQService;
import org.junit.BeforeClass;
import org.junit.Test;

import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.junit.Assert.*;

public class DodgydriversUnitTest {
    private static Registry registry;
    private static DDQService ddqService;  //added to prevent garbage collection from removing the service at random after exporting object

    @BeforeClass
    public static void setup() {
        ddqService = new DDQService();
        try {
            registry = LocateRegistry.createRegistry(1099);
            QuotationService quotationService = (QuotationService)
                    UnicastRemoteObject.exportObject(ddqService, 0);
            registry.bind(Constants.DODGY_DRIVERS_SERVICE, quotationService);
        } catch (Exception e) {
            System.out.println("Trouble: " + e);
        }
    }

    @Test
    public void connectionTest() throws Exception {
        QuotationService service = (QuotationService)
                registry.lookup(Constants.DODGY_DRIVERS_SERVICE);
        assertNotNull(service);
    }

    @Test
    public void generateQuotationTest() throws Exception {
        QuotationService ddqService = new DDQService();

        ClientInfo genQuoTest = new ClientInfo("Niki Collier", 'F', 43, 0, 5, "PQR254/1");
        Quotation actual = ddqService.generateQuotation(genQuoTest);
        Quotation tester = new Quotation("Dodgy Drivers Corp.", "DD001000", 900.0);
        assertThat(actual, instanceOf(Quotation.class));
        assertEquals(tester.company, actual.company);
        assertEquals(tester.reference, actual.reference);
        assertEquals(tester.price, actual.price, 600.00);
    }   /*delta is large since this is only testing quotation generation*/
}
