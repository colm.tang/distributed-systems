package service.core;

import java.io.IOException;
import java.net.InetAddress;
import java.net.URL;

import java.net.UnknownHostException;
import java.util.HashSet;
import java.util.LinkedList;

import javax.jmdns.JmDNS;
import javax.jmdns.ServiceEvent;
import javax.jmdns.ServiceListener;
import javax.jws.WebMethod;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.xml.namespace.QName;
import javax.xml.ws.Endpoint;
import javax.xml.ws.Service;


/**
 * Implementation of the broker service that uses the Service Registry.
 *
 * @author Rem
 *
 */

@WebService
@SOAPBinding(style = SOAPBinding.Style.DOCUMENT, use = SOAPBinding.Use.LITERAL)
public class Broker implements ServiceListener {
    static HashSet<String> paths = new HashSet<>(); //used to store incoming URL's

    public static void main(String[] args) {
        try {
            // Create a JmDNS instance
            JmDNS jmdns = JmDNS.create(InetAddress.getLocalHost());
            // Add a service listener

            Broker broker = new Broker();
            jmdns.addServiceListener("_http._tcp.local.", broker);
            Endpoint.publish("http://0.0.0.0:9000/broker", broker);

            // Wait a bit
            Thread.sleep(30000);
        } catch (UnknownHostException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void serviceAdded(ServiceEvent event) {
        System.out.println("Service added: " + event.getInfo());
    }
    @Override
    public void serviceRemoved(ServiceEvent event) {
        System.out.println("Service removed: " + event.getInfo());
    }
    @Override
    public void serviceResolved(ServiceEvent event) {
        System.out.println("Service resolved: " + event.getInfo());
        String path = event.getInfo().getPropertyString("path");
        if (path != null) {
            try {
                System.out.println("this is : " + path);
                paths.add(path);
            } catch (Exception e) {
                System.out.println("Problem with service: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    @WebMethod
    public LinkedList<Quotation> getQuotations(ClientInfo info) throws Exception {
        LinkedList<Quotation> quotations = new LinkedList<Quotation>();
        QuoterService quotationService = null;

        for(String url : paths) {
            URL wsdlUrl = new URL(url);
            QName serviceName = new QName("http://core.service/", "QuoterService");
            Service service = Service.create(wsdlUrl, serviceName);
            QName portName = new QName("http://core.service/", "QuoterPort");

            quotationService = service.getPort(portName, QuoterService.class);
            quotations.add(quotationService.generateQuotation(info));
        }

        return quotations;
    }
}