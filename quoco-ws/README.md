# Colm Tang		16760705

# Introduction

Solution for practical 2: WS-based Distribution.

# Running the Program

To run the program you run commands (in root folder):

1. `mvn compile`
2. `mvn package`
3. `docker-compose build`
4. `docker-compose up`
5. run `docker-compose down` to cleanup docker network and images.

# Changes made from Quoco.master -> Quoco.rmi

To implement service.listener you must override three methods required to run it. Inside of
serviceResolved I added a Hashset to hold the URL's that connected to be iterated through for
getQuotations(). I would have used a "invokeBrokerService()" method like in the example but ran
into the issue of passing ClientInfo into the method since it serviceResolved is defined outside
the scope of this assignment.
A hashset was used to prevent multiple iterations of the same URL being stored due to broker
removing services and then adding them back after a period of time. This would result in errors
if the client was rerun(this was before docker containerisation).
