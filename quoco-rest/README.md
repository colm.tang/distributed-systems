# Colm Tang		16760705

# Introduction

Solution for Lab 5: REST Services.

# Running the Program

To run the program you run commands (in root folder):

1. `mvn compile`
2. `mvn install`
3. `mvn package`
4. `docker-compose build`
5. `docker-compose up`
6. run `docker-compose down` to cleanup docker network and images.

# Changes made from Quoco.master -> Quoco.rest

Made client a container since it made the execution of the services easier and neater.
Added a sleep command on client because it would start before the other services were ready
and so 6 seconds was added to get it to wait for the other services.
I have changed the url's for connecting each service. This was a change made on the basis
that Docker container names are tied to container IP. 