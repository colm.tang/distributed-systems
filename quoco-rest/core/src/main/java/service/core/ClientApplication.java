package service.core;

import java.util.LinkedList;

public class ClientApplication {
    private ClientInfo info = new ClientInfo();
    private int applicationNumber;
    private LinkedList<Quotation> quotations = new LinkedList<Quotation>();

    public ClientApplication(ClientInfo client, int appNo,LinkedList quotes) {
        info = client;
        applicationNumber = appNo;
        quotations = quotes;
    }

    public ClientApplication() {}

    public ClientInfo getInfo() {
        return info;
    }

    public void setInfo(ClientInfo info) {
        this.info = info;
    }

    public int getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(int applicationNumber) {
        this.applicationNumber = applicationNumber;
    }

    public LinkedList<Quotation> getQuotations() {
        return quotations;
    }

    public void setQuotations(LinkedList<Quotation> quotations) {
        this.quotations = quotations;
    }
}
