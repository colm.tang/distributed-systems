package service.broker;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import service.core.ClientApplication;
import service.core.ClientInfo;
import service.core.Quotation;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Implementation of the broker service that uses the Service Registry.
 * 
 * @author Rem
 *
 */
@RestController
public class LocalBrokerService {
	private HashMap<Integer,ClientApplication> clientApplications = new HashMap<>();
	private int applicationNumber = 0;

	public LinkedList<Quotation> getQuotations(ClientInfo info) {
		LinkedList<Quotation> received = new LinkedList<>();

		RestTemplate restTemplate = new RestTemplate();
		HttpEntity<ClientInfo> request = new HttpEntity<>(info);
		Quotation afqQuote = restTemplate.postForObject("http://auldfellas:8081/quotations",request, Quotation.class);
		Quotation ddqQuote = restTemplate.postForObject("http://dodgydrivers:8082/quotations",request, Quotation.class);
		Quotation gpqQuote = restTemplate.postForObject("http://girlpower:8083/quotations",request, Quotation.class);
		received.add(afqQuote);
		received.add(ddqQuote);
		received.add(gpqQuote);

		return received;
	}

	@RequestMapping(value="/applications",method=RequestMethod.POST)
	public ResponseEntity<ClientApplication> returnQuotations(@RequestBody ClientInfo info) throws URISyntaxException {
		LinkedList<Quotation> quotations = getQuotations(info);
		ClientApplication clientApp = new ClientApplication(info,applicationNumber,quotations);
		clientApplications.put(applicationNumber++,clientApp);
		String path = ServletUriComponentsBuilder.fromCurrentContextPath().build().toUriString()+ "/applications/"+clientApp.getApplicationNumber();
		HttpHeaders headers = new HttpHeaders();
		headers.setLocation(new URI(path));
		return new ResponseEntity<>(clientApp, headers, HttpStatus.CREATED);
	}

	@RequestMapping(value="/applications/{application-number}",method=RequestMethod.GET)
	public ClientApplication getClientApplication(@PathVariable("application-number") int clientAppNum ) {
		return clientApplications.get(clientAppNum);
	}

	@RequestMapping(value="/applications",method=RequestMethod.GET)
	public HashMap<Integer,ClientApplication> getAllApplications() {
		return clientApplications;
	}
}
