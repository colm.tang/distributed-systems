package service.message;

import service.core.QuotationService;

public class Init {
    private QuotationService service;

    public Init(QuotationService qs) {
        this.service = qs;
    }

    public QuotationService getQuotationService() {
        return service;
    }
}
