package service.actor;
import akka.actor.*;
import akka.actor.AbstractActor;
import scala.concurrent.duration.Duration;
import service.core.ClientInfo;
import service.core.Quotation;
import service.messages.*;

import java.util.*;
import java.util.concurrent.TimeUnit;

public class Broker extends AbstractActor {
    Set<ActorRef> actorRefs = new HashSet<>();
    Map<Integer, LinkedList<Quotation>> quoteList = new HashMap<>();
    Map<Integer, ClientInfo> clientList = new HashMap<>();

    int SEED_ID = 0;
    ActorRef clientRef;

    @Override
    public Receive createReceive() {
        return receiveBuilder()
            .match(String.class,
            msg -> {
                if (!msg.equals("register")) return;
                System.out.println("incoming from " + getSender());
                actorRefs.add(getSender()); //registers each service using the msg manually sent from each
            })
            .match(ApplicationRequest.class,
            msg -> {
                quoteList.put(SEED_ID,new LinkedList<>());  // creates new linked list for each clientInfo
                clientList.put(SEED_ID,msg.getClientInfo());
                for(ActorRef ref: actorRefs) {
                    ref.tell(new QuotationRequest(SEED_ID,msg.getClientInfo()),getSelf());
                }
                getContext().system().scheduler().scheduleOnce(
                        Duration.create(2, TimeUnit.SECONDS),
                        getSelf(),
                        new RequestDeadline(SEED_ID++),
                        getContext().dispatcher(), null);
                clientRef = getSender();    // receives AppRequest, sends itself a delayed deadline msg which will be sent in 2 seconds.
            })
            .match(QuotationResponse.class,
            msg -> {
                Quotation newQuote;
                newQuote = msg.getQuotation();
                quoteList.get(msg.getId()).add(newQuote); // adds quotations to the already created linked list attached to corresponding ID
            })
            .match(RequestDeadline.class,   // receives delayed deadline msg and sends quotes that have been received so far
            msg -> {
                clientRef.tell(new ApplicationResponse(clientList.get(msg.getId()),quoteList.get(msg.getId())),getSelf());
            }).build();
    }
}
