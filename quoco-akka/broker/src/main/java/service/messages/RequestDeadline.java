package service.messages;

public class RequestDeadline {
    private int id;

    public RequestDeadline(int identifier) {
        id = identifier;
    }

    public int getId() {
        return id;
    }
}
