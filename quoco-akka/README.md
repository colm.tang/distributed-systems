# Colm Tang		16760705

# Introduction

Solution for Lab 6: AKKA Services.

# Running the Program

To run the program you run commands (in root folder):
Steps 3 to 7 need to be run using separate concurrent terminals.

1. `mvn compile`
2. `mvn install`
3. `mvn exec:java -pl broker`
4. `mvn exec:java -pl auldfellas`
5. `mvn exec:java -pl girlpower`
6. `mvn exec:java -pl dodgydrivers`
7. `mvn exec:java -pl client`
8. output will be viewable from client terminal
