package service.messages;

import service.core.ClientInfo;

public class ApplicationRequest implements MySerializable {
    private ClientInfo clientInfo = new ClientInfo();

    public ApplicationRequest(ClientInfo client) {
        this.clientInfo = client;
    }

    public ApplicationRequest() {}

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo info) {
        this.clientInfo = info;
    }
}
