package service.messages;

import service.core.ClientInfo;
import service.core.Quotation;

import java.util.LinkedList;

public class ApplicationResponse implements MySerializable {
    private ClientInfo clientInfo;
    private LinkedList<Quotation> quotations = new LinkedList<Quotation>();

    public ApplicationResponse(ClientInfo client, LinkedList<Quotation> quotes) {
        this.clientInfo = client;
        this.quotations = quotes;
    }

    public ApplicationResponse() {}

    public ClientInfo getClientInfo() {
        return clientInfo;
    }

    public void setClientInfo(ClientInfo info) {
        this.clientInfo = info;
    }

    public LinkedList<Quotation> getQuotations() {
        return quotations;
    }

    public void setQuotations(LinkedList<Quotation> quotations) {
        this.quotations = quotations;
    }
}
