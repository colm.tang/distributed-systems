package service.actor;

import akka.actor.*;
import service.core.Quotation;
import service.core.QuotationService;
import service.message.Init;
import service.messages.QuotationRequest;
import service.messages.QuotationResponse;

public class Quoter extends AbstractActor {
    private QuotationService service;

    @Override
    public Receive createReceive() {
        return receiveBuilder().
            match(Init.class,
            msg -> {
                System.out.println("Initialized " + msg.getQuotationService());
                service = msg.getQuotationService();
            }).
            match(QuotationRequest.class,
            msg -> {
                System.out.println("Quotation request received");
                Quotation quotation = service.generateQuotation(msg.getClientInfo());
                getSender().tell(new QuotationResponse(msg.getId(), quotation), getSelf());
                System.out.println("Quotation response sent");
            }).build();
    }

}