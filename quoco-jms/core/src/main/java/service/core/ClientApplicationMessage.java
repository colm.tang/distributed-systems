package service.core;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

public class ClientApplicationMessage implements Serializable {
    public ClientInfo info;
    public long id;
    public Set<Quotation> quotations = new LinkedHashSet<Quotation>();

    public ClientApplicationMessage(LinkedHashSet quotations, ClientInfo info, Long id) {
        this.quotations = quotations;
        this.info = info;
        this.id = id;
    }
}
