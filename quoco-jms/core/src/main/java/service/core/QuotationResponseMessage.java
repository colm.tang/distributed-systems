package service.core;

import java.io.Serializable;

public class QuotationResponseMessage implements Serializable {
    public long id;
    public ClientInfo info;
    public Quotation quotation;

    public QuotationResponseMessage(long id, Quotation quotation, ClientInfo info) {
        this.id = id;
        this.info = info;
        this.quotation = quotation;


    }
}
