package service;

import org.apache.activemq.ActiveMQConnectionFactory;
import service.core.*;
import sun.awt.image.ImageWatched;

import javax.jms.*;
import javax.jms.Queue;
import java.util.*;
public class Broker {
    static Map<Long, ClientInfo> cache = new HashMap<Long, ClientInfo>();
    static Map<Long, LinkedHashSet<Quotation>> sortedQuotations = new HashMap<Long, LinkedHashSet<Quotation>>();
    static QuotationResponseMessage storedResponse;

    public static void main(String[] args) {
        String host = "localhost";
        if (args.length > 0) {
            host = args[0];
        }
        try {
            ConnectionFactory factory = new ActiveMQConnectionFactory("failover://tcp://" + host + ":61616");
            Connection connection = factory.createConnection();
            connection.setClientID("broker");
            final Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

            Queue clientToBroker = session.createQueue("CLIENTBROKER");
            Queue brokerToClient = session.createQueue("BROKERCLIENT");

            Queue incomingQuotations = session.createQueue("QUOTATIONS");
            Topic outgoingRequests = session.createTopic("REQUESTS");

            MessageConsumer consumerForClient = session.createConsumer(clientToBroker);
            MessageProducer producerForServices = session.createProducer(outgoingRequests);
            final MessageConsumer consumerOfQuotations = session.createConsumer(incomingQuotations);
            final MessageProducer producerForClient = session.createProducer(brokerToClient);

            connection.start();

            new Thread(new Runnable() { //separate thread to listen for replies from the 3 services
                public void run() {
                    while (true) {
                        try {
                            Message receiveQuotation = consumerOfQuotations.receive();
                            if (receiveQuotation instanceof ObjectMessage) {
                                Object content = ((ObjectMessage) receiveQuotation).getObject();
                                System.out.println("quotation received");
                                if (content instanceof QuotationResponseMessage) {
                                    QuotationResponseMessage response = (QuotationResponseMessage) content;
                                    storedResponse = response;
                                    System.out.println("added to list");
                                    sortedQuotations.get(response.id).add(response.quotation);
                                }
                                receiveQuotation.acknowledge();
                            } else {
                                System.out.println("Unknown message type: " +
                                        receiveQuotation.getClass().getCanonicalName());
                            }
                            boolean checkIfFinished = true;    // with added services, increase the expected .size() for if, i.e. 5 services = 5 quotations
                            for (Map.Entry<Long,LinkedHashSet<Quotation>> entry : sortedQuotations.entrySet()) {
                                if(entry.getValue().size() != 3) {
                                    checkIfFinished = false;
                                }
                            }
                            if (checkIfFinished) {
                                for(Map.Entry<Long,LinkedHashSet<Quotation>> entry : sortedQuotations.entrySet()) {
                                    Message quotationForClient = session.createObjectMessage(new ClientApplicationMessage(entry.getValue(),cache.get(entry.getKey()),entry.getKey()));
                                    producerForClient.send(quotationForClient);
                                    //System.out.println("Sent to client");
                                }
                            }

                        } catch (JMSException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }).start();

            while (true) {
                // Get the next message from the CLIENTBROKER queue
                Message receiveRequest = consumerForClient.receive();
                // Check it is the right type of message
                if (receiveRequest instanceof ObjectMessage) {
                    // It’s an Object Message
                    Object content = ((ObjectMessage) receiveRequest).getObject();
                    System.out.println("Client request received");
                    if (content instanceof QuotationRequestMessage) {
                        // It’s a Quotation Request Message
                        QuotationRequestMessage request = (QuotationRequestMessage) content;

                        Message forwardRequest = session.createObjectMessage(new QuotationRequestMessage(request.id, request.info));
                        cache.put(request.id, request.info);
                        sortedQuotations.put(request.id, new LinkedHashSet<Quotation>());
                        producerForServices.send(forwardRequest);
                        System.out.println("published on topic");   //broadcast message to all subscribers of topic REQUESTS
                    }
                    receiveRequest.acknowledge();
                } else {
                    System.out.println("Unknown message type: " +
                            receiveRequest.getClass().getCanonicalName());
                }
            }
        } catch (JMSException e) {
            e.printStackTrace();
        }
    }
}
