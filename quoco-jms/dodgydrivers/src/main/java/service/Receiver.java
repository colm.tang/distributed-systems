package service;

import org.apache.activemq.ActiveMQConnectionFactory;
import service.core.Quotation;
import service.core.QuotationRequestMessage;
import service.core.QuotationResponseMessage;
import service.dodgydrivers.DDQService;

import javax.jms.*;

public class Receiver {
    private static DDQService ddqService = new DDQService();

    public static void main(String[] args) {
        String host = "localhost";
        if(args.length > 0) {
            host = args[0];
        }
        try {
            ConnectionFactory factory = new ActiveMQConnectionFactory("failover://tcp://"+host+":61616");
            Connection connection = factory.createConnection();
            connection.setClientID("dodgydrivers");
            Session session = connection.createSession(false, Session.CLIENT_ACKNOWLEDGE);

            Queue outgoing = session.createQueue("QUOTATIONS");
            Topic incoming = session.createTopic("REQUESTS");
            MessageConsumer consumer = session.createConsumer(incoming);
            MessageProducer producer = session.createProducer(outgoing);

            connection.start();
            while (true) {
                // Get the next message from the REQUESTS topic
                Message message = consumer.receive();
                System.out.println("received unknown");
                // Check it is the right type of message
                if (message instanceof ObjectMessage) {
                    // It’s an Object Message
                    Object content = ((ObjectMessage) message).getObject();
                    System.out.println("request verified");
                    if (content instanceof QuotationRequestMessage) {
                        // It’s a Quotation Request Message
                        QuotationRequestMessage request = (QuotationRequestMessage) content;
                        // Generate a quotation and send a quotation response message…
                        Quotation quotation = ddqService.generateQuotation(request.info);
                        Message response = session.createObjectMessage(new QuotationResponseMessage(request.id, quotation, request.info));
                        producer.send(response);
                        System.out.println("quotation sent to broker");
                    }
                    message.acknowledge();
                } else {
                    System.out.println("Unknown message type: " +
                            message.getClass().getCanonicalName());
                }
            }
        } catch (JMSException e) { e.printStackTrace(); }
    }

}
