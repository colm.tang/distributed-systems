# Colm Tang		16760705

# Introduction

Solution for Lab 4: Message Oriented Middleware.

# Running the Program

To run the program you run commands (in root folder):

1. `mvn compile`
2. `mvn package`
3. `docker-compose build`
4. `docker-compose up`
5. run `docker-compose down` to cleanup docker network and images.

# Changes made from Quoco.master -> Quoco.jms

Included ClientInfo into ClassApplicationMessage due to the assignment wording but I believe
the cache in broker and client could be used to identify clients after sending.
The code is commented and has several print statements used to identify which part of the 
process the program currently is at.
Used linkedHashSet to contain the quotations returned from services until they were ready to
send. This also helped with identifying which quotes were intended for which clients when
used in conjunction with cache. This way i could get around creating timeouts.